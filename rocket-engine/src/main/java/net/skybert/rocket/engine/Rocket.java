package net.skybert.rocket.engine;

public class Rocket {
  String color;

  public Rocket(final String pColor) {
    color = pColor;
  }

  public String getColor() {
    return color;
  }

}
