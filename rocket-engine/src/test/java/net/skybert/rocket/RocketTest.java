package net.skybert.rocket;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import net.skybert.rocket.engine.Rocket;

/**
 * RocketTest
 */
public class RocketTest {

  @Test
  public void canCreateRedRocket() {
    Rocket rocket = new Rocket("red");
    assertEquals("red", rocket.getColor());
  }
}
