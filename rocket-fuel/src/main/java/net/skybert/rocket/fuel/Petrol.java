package net.skybert.rocket.fuel;

/**
 * Petrol
 */
public class Petrol {
  private int litres;

  public Petrol(final int pLitres) {
    litres = pLitres;
  }

  public int getLitres() {
    return litres;
  }

}