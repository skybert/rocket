package net.skybert.rocket.fuel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * Petrol test
 */
public class PetrolTest {
  @Test
  public void canCreateSomePetrol() {
    Petrol petrol = new Petrol(2);
    assertEquals(2, petrol.getLitres());
  }

}